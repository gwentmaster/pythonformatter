#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-11-13 17:44:32
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


import os
import time
from threading import Thread

import sublime_plugin

from .my_isort import MyIsort


class PythonFormatterListener(sublime_plugin.EventListener):

    last_save = 0
    view = None
    exclude = {
        "C:/Users/exceed/Envs",
        "C:/Users/exceed/Downloads/bitbucket",
        "D:/programs/z/miniconda",
        "D:/programs/Sublime Text/Data",
        "C:/Users/exceed/Desktop/new/plugins"
    }

    def __init__(self):

        self.exclude = {os.path.abspath(i) for i in self.exclude}

    def _run_task(self, func, *args, **kwargs):

        thread = Thread(target=func, args=args, kwargs=kwargs, daemon=True)
        thread.start()

    def use_isort(self):

        file = os.path.abspath(self.view.file_name())

        temp_file = ""
        for i in file:
            if i == os.path.sep:
                if temp_file in self.exclude:
                    return None
            temp_file += i

        MyIsort().sort(file)

    def on_post_save(self, view):

        if not view.match_selector(0, "source.python"):
            return None

        self.last_save, pre_save = time.time(), self.last_save
        if self.last_save - pre_save < 1:
            return None

        self.view = view

        self._run_task(self.use_isort)
