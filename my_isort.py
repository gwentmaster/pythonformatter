#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-11-16 10:40:21
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


import os
import re


KNOWN_STANDARD_LIBRARY = [
    "AL", "BaseHTTPServer", "Bastion", "CGIHTTPServer", "Carbon",
    "ColorPicker", "ConfigParser", "Cookie", "DEVICE", "DocXMLRPCServer",
    "EasyDialogs", "FL", "FrameWork", "GL", "HTMLParser", "MacOS",
    "MimeWriter", "MiniAEFrame", "Nav", "PixMapWrapper", "Queue",
    "SUNAUDIODEV", "ScrolledText", "SimpleHTTPServer", "SimpleXMLRPCServer",
    "SocketServer", "StringIO", "Tix", "Tkinter", "UserDict", "UserList",
    "UserString", "W", "__builtin__", "abc", "aepack", "aetools", "aetypes",
    "aifc", "al", "anydbm", "applesingle", "argparse", "array", "ast",
    "asynchat", "asyncio", "asyncore", "atexit", "audioop", "autoGIL",
    "base64", "bdb", "binascii", "binhex", "bisect", "bsddb", "buildtools",
    "builtins", "bz2", "cPickle", "cProfile", "cStringIO", "calendar", "cd",
    "cfmfile", "cgi", "cgitb", "chunk", "cmath", "cmd", "code", "codecs",
    "codeop", "collections", "colorsys", "commands", "compileall", "compiler",
    "concurrent", "configparser", "contextlib", "contextvars", "cookielib",
    "copy", "copy_reg", "copyreg", "crypt", "csv", "ctypes", "curses",
    "dataclasses", "datetime", "dbhash", "dbm", "decimal", "difflib",
    "dircache", "dis", "distutils", "dl", "doctest", "dumbdbm", "dummy_thread",
    "dummy_threading", "email", "encodings", "ensurepip", "enum", "errno",
    "exceptions", "faulthandler", "fcntl", "filecmp", "fileinput",
    "findertools", "fl", "flp", "fm", "fnmatch", "formatter", "fpectl",
    "fpformat", "fractions", "ftplib", "functools", "future_builtins", "gc",
    "gdbm", "gensuitemodule", "getopt", "getpass", "gettext", "gl", "glob",
    "grp", "gzip", "hashlib", "heapq", "hmac", "hotshot", "html",
    "htmlentitydefs", "htmllib", "http", "httplib", "ic", "icopen", "imageop",
    "imaplib", "imgfile", "imghdr", "imp", "importlib", "imputil", "inspect",
    "io", "ipaddress", "itertools", "jpeg", "json", "keyword", "lib2to3",
    "linecache", "locale", "logging", "lzma", "macerrors", "macostools",
    "macpath", "macresource", "mailbox", "mailcap", "marshal", "math", "md5",
    "mhlib", "mimetools", "mimetypes", "mimify", "mmap", "modulefinder",
    "msilib", "msvcrt", "multifile", "multiprocessing", "mutex", "netrc",
    "new", "nis", "nntplib", "numbers", "operator", "optparse", "os",
    "ossaudiodev", "parser", "pathlib", "pdb", "pickle", "pickletools",
    "pipes", "pkgutil", "platform", "plistlib", "popen2", "poplib", "posix",
    "posixfile", "pprint", "profile", "pstats", "pty", "pwd", "py_compile",
    "pyclbr", "pydoc", "queue", "quopri", "random", "re", "readline",
    "reprlib", "resource", "rexec", "rfc822", "rlcompleter", "robotparser",
    "runpy", "sched", "secrets", "select", "selectors", "sets", "sgmllib",
    "sha", "shelve", "shlex", "shutil", "signal", "site", "sitecustomize",
    "smtpd", "smtplib", "sndhdr", "socket", "socketserver", "spwd", "sqlite3",
    "ssl", "stat", "statistics", "statvfs", "string", "stringprep", "struct",
    "subprocess", "sunau", "sunaudiodev", "symbol", "symtable", "sys",
    "sysconfig", "syslog", "tabnanny", "tarfile", "telnetlib", "tempfile",
    "termios", "test", "textwrap", "this", "thread", "threading", "time",
    "timeit", "tkinter", "token", "tokenize", "trace", "traceback",
    "tracemalloc", "ttk", "tty", "turtle", "turtledemo", "types", "typing",
    "unicodedata", "unittest", "urllib", "urllib2", "urlparse",
    "usercustomize", "uu", "uuid", "venv", "videoreader", "warnings", "wave",
    "weakref", "webbrowser", "whichdb", "winreg", "winsound", "wsgiref",
    "xdrlib", "xml", "xmlrpc", "xmlrpclib", "zipapp", "zipfile", "zipimport",
]


def load_third_party_libs():

    venv_dir = "C:/Users/exceed/Envs/test/Lib/site-packages"
    thp_libs = set()
    for f in os.listdir(venv_dir):
        if os.path.isdir("{}/{}".format(venv_dir, f)):
            if (
                f.endswith("dist-info")
                or f.endswith("egg-info")
                or (f == "__pycache__")
            ):
                continue
        else:
            if not f.endswith(".py"):
                continue
            f = f.rstrip(".py")
        thp_libs.add(f)
    return list(thp_libs)


KNOWN_THIRD_PARTY_LIBRARY = load_third_party_libs()


class ImportAnalyser(object):

    def __init__(self):

        self.from_no_bracket_pattern = re.compile(
            r"^from[ \f\t]+([\.\w]+)[ \f\t]+import[ \f\t]+([,\s\w\*]+)\s*"
        )
        self.from_bracket_pattern = re.compile(
            r"^from[ \f\t]+([\.\w]+)[ \f\t]+import[ \f\t]*\("
            + r"([,\s\w\*]+)\)\s*"
        )
        self.import_pattern = re.compile(r"^import[ \f\t]+([,\s\.\w]+)\s*")

        self.as_pattern = re.compile(r"([\w\.]+)[ \f\t]+as[ \f\t]+([\w]+)")

    def analyse(self, statement):

        from_match = (
            self.from_bracket_pattern.match(statement)
            or self.from_no_bracket_pattern.match(statement)
        )
        if from_match:
            package = from_match.group(1)
            children = set()
            for child in from_match.group(2).split(","):
                child = child.strip()
                if not child:
                    continue
                children.add(child)
            children = {"*"} if "*" in children else children
            return [
                ImportStatement(
                    package=package,
                    children=children,
                    raw_statement=statement
                )
            ]

        import_match = self.import_pattern.match(statement)
        if import_match is None:
            raise RuntimeError(
                "wrong import statement: {!r}".format(statement)
            )
        state_objs = []
        for nick_package in import_match.group(1).split(","):
            nick_package = nick_package.strip()
            if not nick_package:
                continue
            as_match = self.as_pattern.match(nick_package)
            if as_match is None:
                state_objs.append(
                    ImportStatement(package=nick_package)
                )
            else:
                state_objs.append(
                    ImportStatement(
                        package=as_match.group(1),
                        nickname=as_match.group(2)
                    )
                )
        if len(state_objs) == 1:
            state_objs[0] = state_objs[0].copy_with(raw_statement=statement)
        return state_objs


class ImportStatement(object):

    def __init__(
        self,
        package=None,
        nickname=None,
        children=None,
        raw_statement=None
    ):

        self._package = package
        self._nickname = nickname
        self._children = set() if children is None else children
        self._raw_statement = raw_statement

    def _gen_statement(self):

        self._overlength = False

        if self.children:
            from_stm = "from {} import ".format(self.package)
            children = sorted(self.children, key=lambda x: x.lower())
            children_stm = ", ".join(children)
            if len(from_stm + children_stm) < 80:
                self._statement = from_stm + children_stm
                return None
            self._overlength = True
            children_stm = ",\n    ".join(children)
            self._statement = from_stm + "(\n    " + children_stm + "\n)"
            return None

        if self.nickname is not None:
            self._statement = "import {} as {}".format(
                self.package,
                self.nickname
            )
            return None

        self._statement = "import {}".format(self.package)

    @property
    def root_package(self):
        return self._package.split(".")[0]

    @property
    def package(self):
        return self._package

    @property
    def nickname(self):
        return self._nickname

    @property
    def nick_package(self):
        if self._nickname is None:
            return self._package
        return "{} as {}".format(self._package, self._nickname)

    @property
    def children(self):
        return self._children

    @property
    def overlength(self):
        if hasattr(self, "_overlength"):
            return self._overlength
        self._gen_statement()
        return self._overlength

    @property
    def statement(self):

        self._gen_statement()
        return self._statement

    @property
    def raw_statement(self):
        return self._raw_statement

    @property
    def package_source(self):
        if hasattr(self, "_package_source"):
            return self._package_source
        if self.root_package in KNOWN_STANDARD_LIBRARY:
            self._package_source = 1
            return 1
        if self.root_package in KNOWN_THIRD_PARTY_LIBRARY:
            self._package_source = 2
            return 2
        else:
            return 3

    def merge(self, other):

        if self.nick_package != other.nick_package:
            raise RuntimeError("can't merge `{}` and `{}`".format(
                self.nick_package,
                other.nick_package
            ))
        if bool(self.children) + bool(other.children) == 1:
            raise RuntimeError("can't merge import and from_import")

        self._children = set(self._children)
        self._children.update(other.children)
        self._children = {"*"} if "*" in self._children else self._children

        self._raw_statement = self._raw_statement or other._raw_statement

    def copy_with(
        self,
        package=None,
        nickname=None,
        children=None,
        raw_statement=None
    ):
        package = self.package if package is None else package
        nickname = self.nickname if nickname is None else nickname
        children = self.children if children is None else children
        raw_statement = (
            self._raw_statement if raw_statement is None
            else raw_statement
        )
        return ImportStatement(
            package=package,
            nickname=nickname,
            children=children,
            raw_statement=raw_statement
        )

    def __gt__(self, other):

        if not isinstance(other, ImportStatement):
            raise TypeError(
                "compare with {!r} not supported".format(type(other).__name__)
            )

        # 自定义>第三方>标准库
        if self.package_source != other.package_source:
            return self.package_source > other.package_source

        # from...import>import
        if bool(self.children) != bool(other.children):
            return True if bool(self.children) else False

        # 多行>单行
        if self.overlength != other.overlength:
            return True if self.overlength else False

        # "Z">"a"
        return (self.package).lower() > (other.package).lower()


class MyIsort(object):

    def __init__(self):

        self.import_analyser = ImportAnalyser()

    def _analyse_file(self, file):

        no_imports = {"before": "", "after": ""}
        imports = []
        import_flag = "before"
        in_import = False
        with open(file, mode="r", encoding="utf-8") as f:
            for line in f.readlines():
                if in_import is True:
                    imports[-1] += line
                    in_import = False if ")" in line else in_import
                else:
                    if line.startswith("import") or line.startswith("from"):
                        import_flag = "after"
                        imports.append(line)
                        in_import = True if "(" in line else in_import
                    else:
                        no_imports[import_flag] += line
        return no_imports, imports

    def _analyse_imports(self, imports):

        import_dic = {}
        imp_objs = []
        for i in imports:
            imp_objs.extend(self.import_analyser.analyse(i))

        for imp in imp_objs:
            key = (imp.nick_package, bool(imp.children))
            if key not in import_dic:
                import_dic[key] = imp
                continue
            import_dic[key].merge(imp)

        sorted_imp_objs = sorted(import_dic.values())
        if len(imp_objs) == len(sorted_imp_objs):
            for i, j in zip(imp_objs, sorted_imp_objs):
                if i.raw_statement is None:
                    break
                if i.raw_statement.strip() != j.statement:
                    break
            else:
                return None

        current_source = None
        import_statement = ""
        for imp in sorted_imp_objs:
            if imp.package_source != current_source:
                if current_source is not None:
                    import_statement += "\n"
                current_source = imp.package_source
            import_statement += "{}\n".format(imp.statement)
        import_statement = import_statement[:-1]

        return import_statement

    def sort(self, file):

        no_imports, imports = self._analyse_file(file)
        import_statement = self._analyse_imports(imports)

        if import_statement is None:
            return None

        for char in no_imports["after"]:
            if char != "\n":
                conj = "\n\n\n"
                break
        else:
            conj = "\n"
        with open(file, "wb") as f:
            f.write((
                no_imports["before"]
                + import_statement
                + conj
                + no_imports["after"].lstrip("\n")
            ).encode("utf-8"))
